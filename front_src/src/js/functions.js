jQuery(document).ready(function($) {

	(function (){

		var janelaLargura = $(window).width();	
		if(janelaLargura <= 768){
			var menuLink = $('[data-submenu]');
			menuLink.on('click', 'a', function(event) {
				event.preventDefault();
				$(this).next('.submenu').slideToggle(400);
			});
		}
		
	})();

	(function (){
		var carousel = $('[data-carousel]');
		var productCarousel = $('[data-slider="product"]');
		carousel.owlCarousel({
			navigation : false, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay:true,
			pagination:false
		});


		productCarousel.owlCarousel({
			navigation : false, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			autoPlay:true,
			pagination:false,
			items:3
		});
		$('[data-slider-btn="next"]').click(function(){
			productCarousel.trigger('owl.next');
		});
		$('[data-slider-btn="prev"]').click(function(){
			productCarousel.trigger('owl.prev');
		});
	})();
 		
});
