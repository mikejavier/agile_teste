<?php
//Chave de acesso a API
define("TOKEN", "5V92KsTeIBjEEujlQCifKw91iDBWAi");

//URL das imagens do site
define("URL_IMG", "http://www.casasfreire.com.br/files/");

//Função que conecta com a API para requisições com GET
function get($metodo){
	$ch = curl_init("http://www.agileecommerce.com.br/app/api/" . $metodo);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_POSTFIELDS, '');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	  'Content-Type: application/json',
	  'Token: '. TOKEN)
	);
	return curl_exec($ch);
}

//Função para formatar valor no padrão Brasil
function FormatarValorExibir($valor){
	if(isset($valor)){
	    return number_format($valor,2,",",".");
	}
	else{
		return "0,00";
	}
}

//Consulta de categorias do menu
$categorias = json_decode(get('ConsultarCategorias?&codcategoriapai=1&order=nome'), true);
if(count($categorias[categorias]) > 0){
	foreach ($categorias[categorias] as $categoria){
		echo $categoria[nome] . "<br>";		
	}
}

//Consulta de produtos em promoção
$produtos = json_decode(get('ConsultarProdutos?promocao=S&limit=10'), true);
if(count($produtos[produtos]) > 0){
	foreach ($produtos[produtos] as $produto){
		echo URL_IMG . 'large/' . $produto[imagem] . "<br>";
		echo $produto[nome] . "<br>";
		echo FormatarValorExibir($produto[preco]) . "<br>";
		echo FormatarValorExibir($produto[precopromocao]) . "<br>";
	}
}
?>