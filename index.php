<?php
    //Chave de acesso a API
    define("TOKEN", "5V92KsTeIBjEEujlQCifKw91iDBWAi");

    //URL das imagens do site
    define("URL_IMG", "http://www.casasfreire.com.br/files/");

    //Função que conecta com a API para requisições com GET
    function get($metodo){
        $ch = curl_init("http://www.agileecommerce.com.br/app/api/" . $metodo);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Token: '. TOKEN)
        );
        return curl_exec($ch);
    }

    //Função para formatar valor no padrão Brasil
    function FormatarValorExibir($valor){
        if(isset($valor)){
            return number_format($valor,2,",",".");
        }
        else{
            return "0,00";
        }
    }
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- FAVICON E TOUCH ICON IN THE ROOT DIRECTORY -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="dist/css/styles.combined.min.css">

        <!-- HUMANS -->
        <link type="text/plain" rel="author" href="humans.txt" />

        <!-- OPEN GRAPH -->
        <meta property='og:title' content='Título do site' />
        <meta property='og:description' content='Um breve resumo da descrição do site' />
        <meta property='og:url' content='http://url-do-site' />
        <meta property='og:image' content='http://url-do-site/img/img-og-facebook-200x200.png'/>
        <meta property='og:type' content='website' />
        <meta property='og:site_name' content='Nome do site' />

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=750042738419021";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <header class="header">
            <div class="header_top">
                <div class="wrap">
                    <nav class="nav-top">
                        <a href="#" class="nav-top_item">Minha conta</a>
                        <a href="#" class="nav-top_item">Meus pedidos</a>
                        <a href="#" class="nav-top_item">Fale conosco</a>
                    </nav><!-- /.nav-top -->
                    <div class="user-login">
                        Olá, <span class="user-login_label">Pedro</span>
                        <a href="#" class="user-login_logout">Sair</a>
                    </div><!-- /.user-login -->
                </div>
            </div><!-- /.header_top -->

            <div class="header_main">
                <div class="wrap">
                    <div class="logo">logo da empresa</div>
                    <div class="header_main_inputs">
                        <form action="#" class="search">
                            <input type="text" class="input" placeholder="O que você está buscando?">
                            <button type="submit" class="btn-in"><i class="icon-search"></i></button>
                        </form><!-- /.search -->
                        <div class="cart">
                            <div class="cart_btn">
                                <i class="icon-cart"></i>
                                <span class="badge">2</span>
                            </div>
                            <span class="cart_label">R$ 320,00</span>    
                        </div><!-- /.cart -->
                    </div><!-- /.header_main_inputs -->
                </div>
            </div><!-- /.header_main -->

            
            <nav class="menu-container">
                <div class="wrap">
                    <input type="checkbox" id="menu-hamburguer" class="menu-hamburguer-checkbox">
                    <label for="menu-hamburguer" class="menu-hamburguer-label"><i class="icon-menu"></i></label>
                    <label for="menu-hamburguer" class="menu-hamburguer-overlay"></label>

                    <ul class="menu">
                        <?php 
                            $categorias = json_decode(get('ConsultarCategorias?&codcategoriapai=1&order=nome'), true);
                            if(count($categorias[categorias]) > 0):
                                foreach ($categorias[categorias] as $categoria):
                        ?>
                            <li class="menu_item"><a href="#" class="menu_item_link"><?php echo $categoria[nome]; ?></a></li>
                        <?php 
                            endforeach; endif;
                        ?>
                        <!-- <li class="menu_item" data-submenu="true">
                            <a href="#" class="menu_item_link" >Eletronica</a>
                            <ul class="submenu">
                                <li class="menu_item"><a href="#" class="menu_item_link">Linha Completa</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Chuteiras</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Linha Training</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Linha Performance</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Chinelos e Sandálias</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Mais</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Camisas de Times</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Camisetas</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Camisas Polo</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Bermudas</a></li>
                                <li class="menu_item"><a href="#" class="menu_item_link">Jaquetas e Blusões</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </nav>                                                          
        </header>

        <section class="slider">
            <div class="slider_carousel" data-carousel="true">
                <img src="dist/images/slider.jpg" alt="">
                <img src="dist/images/slider.jpg" alt="">
                <img src="dist/images/slider.jpg" alt="">
            </div>
        </section>
        <div class="newsletter">
            <div class="wrap">
                <h2 class="newsletter_title"><i class="icon-newsletter"></i> Receba Novidades</h2>
                <form action="" class="form-inline">
                    <span class="form-control">
                        <input type="text" class="input" placeholder="Nome" required>
                    </span>
                    <span class="form-control">
                        <input type="email" class="input" placeholder="E-mail" required>
                    </span>
                    <span class="form-control">
                        <button type="submit" class="btn">cadastrar</button>
                    </span>
                </form>
            </div>    
        </div>

        <main>
            <div class="wrap">
                <aside class="aside">
                    <nav class="menu-aside">
                        <h2 class="menu-aside_title">Categorias</h2>
                        <?php 
                            $categorias = json_decode(get('ConsultarCategorias?&codcategoriapai=1&order=nome'), true);
                            if(count($categorias[categorias]) > 0):
                                foreach ($categorias[categorias] as $categoria):
                        ?>
                            <a href="#" class="menu-aside_item"><?php echo $categoria[nome]; ?></a>
                        <?php 
                            endforeach; endif;
                        ?>
                    </nav>
                    <div class="aside_ads">
                        <img src="dist/images/publicidade_lateral.jpg" alt="">
                    </div>
                </aside>
                <section class="content">
                    <section class="section">
                        <h1 class="section_title"><span>Novidades</span></h1>
                        <div class="product-slider">
                            <section class="product" data-slider="product">
                                <?php 
                                $produtos = json_decode(get('ConsultarProdutos?limit=10'), true);
                                if(count($produtos[produtos]) > 0):
                                    foreach ($produtos[produtos] as $produto):
                                    $varloProdParcelasx4 = $produto[preco]/4;
                                ?>

                                    <article class="product_item">
                                        <img src="<?php echo URL_IMG . 'large/' . $produto[imagem]; ?>" alt="" class="product_image">
                                        <h3 class="product_title"><?php echo $produto[nome]; ?></h3>
                                        <span class="product_price">
                                            <?php echo FormatarValorExibir($produto[preco]); ?>
                                            <small>4x de R$ <?php echo FormatarValorExibir($varloProdParcelasx4); ?></small>
                                        </span>
                                    </article>
                                    
                                <?php 
                                    endforeach; 
                                    endif;
                                ?>
                            </section>
                            <div class="product-slider_controls">
                                <a class="slider_controls_item" data-slider-btn="prev"><i class="icon-arrow"></i></a>
                                <a class="slider_controls_item" data-slider-btn="next"><i class="icon-arrow"></i></a>
                            </div>
                        </div>
                    </section>    
                    <section class="section publish-content">
                        <div class="publish-content_item">
                            <img src="dist/images/publicidade_content.jpg" alt="">    
                        </div>
                        <div class="publish-content_item">
                            <img src="dist/images/publicidade_content.jpg" alt="">    
                        </div>
                    </section>    
                    <section class="section">
                        <h1 class="section_title"><span>Promoções</span></h1>
                        <section class="product">
                            <?php 
                                $produtos = json_decode(get('ConsultarProdutos?promocao=S&limit=3'), true);
                                if(count($produtos[produtos]) > 0):
                                    foreach ($produtos[produtos] as $produto):
                                    $varloProdParcelasx4 = $produto[preco]/4;
                            ?>

                                <article class="product_item">
                                    <img src="<?php echo URL_IMG . 'large/' . $produto[imagem]; ?>" alt="" class="product_image">
                                    <h3 class="product_title"><?php echo $produto[nome]; ?></h3>
                                    <span class="product_price">
                                        <?php echo FormatarValorExibir($produto[preco]); ?>
                                        <small>4x de R$ <?php echo FormatarValorExibir($varloProdParcelasx4); ?></small>
                                    </span>
                                </article>
                                
                            <?php 
                                endforeach; 
                                endif;
                            ?>
                        </section>
                    </section>    
                </section>
            </div>
        </main>

        <footer class="footer">
            <div class="wrap">
                <div class="footer_item">
                    <h2 class="footer_item_title">Institucional</h2>
                    <nav class="footer-nav">
                        <a href="#" class="footer-nav_item">Quem Somos</a>
                        <a href="#" class="footer-nav_item">Nossas Lojas</a>
                        <a href="#" class="footer-nav_item">Política de Privacidade</a>
                        <a href="#" class="footer-nav_item">Trocas e Devoluções</a>
                        <a href="#" class="footer-nav_item">Frete e Logística</a>
                    </nav>
                </div>
                <div class="footer_item">
                    <h2 class="footer_item_title">Newsletter</h2>
                    <form action="" class="form">
                        <span class="form-control">
                            <input type="text" class="input" placeholder="Nome" required>
                        </span>
                        <span class="form-control">
                            <input type="email" class="input" placeholder="E-mail" required>
                        </span>
                        <span class="form-control">
                            <button type="submit" class="btn">cadastrar</button>
                        </span>
                    </form>
                </div>
                <div class="footer_item">
                    <div class="fb-page" data-href="https://www.facebook.com/AgileEcommerce/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/AgileEcommerce/"><a href="https://www.facebook.com/AgileEcommerce/">Agile E-commerce</a></blockquote></div></div>    
                </div>
            </div>
            <div class="footer-credits">
                <div class="wrap">
                    NOME DA EMPRESA - Todos os direitos reservados
                </div>
            </div>
        </footer>


        <!-- JS -->
        <script src="dist/js/scripts.combined.min.js" async ></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
